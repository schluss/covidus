from django.db import models
from django.forms import ModelForm
from django.urls import reverse
from django.contrib.auth.models import User


class PublicKey(models.Model):
    id = models.AutoField(primary_key=True)
    key = models.TextField(max_length=4096)

    def get_absolute_url(self):
        return reverse('contact-list')


class Organisation(models.Model):
    id = models.AutoField(primary_key=True)
    owner = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              default=1)
    kvk = models.IntegerField()
    street = models.CharField(max_length=256)
    postal_code = models.CharField(max_length=128)
    name = models.CharField(max_length=256)
    tel = models.IntegerField()
    # Is dit niet een OneToOneField ?
    public_key = models.ForeignKey(PublicKey,
                                   on_delete=models.CASCADE,
                                   default='foo')
    # public_key = models.TextField(max_length=512, blank=True, null=True)

    def get_absolute_url(self):
        return reverse('organisation-update', kwargs={'pk': self.pk})


class OrganisationForm(ModelForm):
    class Meta:
        model: Organisation
        fields = ['name', 'kvk', 'street', 'postal_code', 'tel', 'public_key']


class Contact(models.Model):
    id = models.AutoField(primary_key=True)
    data = models.TextField(max_length=1024)
    pub_key = models.ForeignKey(
        PublicKey,
        on_delete=models.CASCADE,
        related_name='public_key_of',
        default=1
    )
    org = models.ForeignKey(
        Organisation,
        on_delete=models.CASCADE,
        related_name='organisation_of')

    def get_absolute_url(self):
         return reverse('success-page')
