from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import forms, widgets
from django.http import HttpResponseNotFound
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from app.models import Organisation, PublicKey, Contact
from django.shortcuts import render
from django.contrib.auth.models import User


def profile(request):
    if request.user.is_authenticated:
        user = request.user
        orgs = Organisation.objects.filter(owner_id=user.id)
        return render(request, 'registration/user_profile.html', {"user": user, "orgs": orgs})
    else:
        context = None
        return render(request, 'registration/user_profile_not_loggedin.html', context)


class PublicKeyCreate(LoginRequiredMixin, CreateView):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    model = PublicKey
    fields = ['key']


class OrganisationList(LoginRequiredMixin, ListView):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    model = Organisation
    paginate_by = 10


class OrganisationCreate(LoginRequiredMixin, CreateView):
    login_url = '/accounts/login/'
    model = Organisation
    fields = ['id', 'name', 'kvk', 'street', 'postal_code', 'tel']

    def form_valid(self, form):
       # we are saving an organisation and getting a new fresh public
       # key from the database. These keys preexists in the database.
       # The database id of the organisation equals the count of the
       # number of organisations. And the id of the organisation will
       # be equal to the id of the public key.
       # So we get the count of the number of orgs already in the database
       # then we add 1. This will get the id of the current organisation we
       # are adding. Then we will get the a priori generated public key of
       # count + 1 from the PublicKey table. And save the id number of this
       # key as the foreignkey.
        form.instance.owner = self.request.user
        this_instance_id = Organisation.objects.count()+1
        pubkey = PublicKey.objects.get(pk=this_instance_id)
        form.instance.public_key = pubkey
        return super().form_valid(form)


class OrganisationDetail(LoginRequiredMixin, DetailView):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    model = Organisation


class OrganisationUpdate(LoginRequiredMixin, UpdateView):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    model = Organisation
    fields = ['name', 'kvk', 'street', 'postal_code', 'tel', 'public_key']


class OrganisationDelete(LoginRequiredMixin, DeleteView):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    model = Organisation
    success_url = reverse_lazy('organisation-list')


class ContactCreate(CreateView):
    model = Contact
    fields = ['data', 'pub_key']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['public_key'] = PublicKey.objects.get(pk=self.kwargs['pk'])
        return context

    def get_initial(self, *args, **kwargs):
        initial = super(ContactCreate, self).get_initial()
        pub_key = PublicKey.objects.get(pk=self.kwargs['pk'])
        initial['pub_key'] = PublicKey.objects.get(pk=self.kwargs['pk'])
        return initial

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['pub_key'].widget = widgets.HiddenInput()
        return form

    def form_valid(self, form):
        org = Organisation.objects.get(pk=self.kwargs['pk'])
        form.instance.org_id = org.id
        return super().form_valid(form)


class ContactList(LoginRequiredMixin,ListView):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    queryset = Contact.objects.all()
    paginate_by = 500

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_contacts'] = self.queryset.filter().count()
        return context


def mainpage(request):
    context = None
    return render(request, 'app/index.html', context)


def success(request):
    context = None
    return render(request, 'app/success.html', context)
