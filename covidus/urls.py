"""covidus URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from app.views import OrganisationCreate, OrganisationUpdate, \
    OrganisationDelete, OrganisationList, profile, ContactCreate, \
    ContactList, mainpage, PublicKeyCreate, success

urlpatterns = [
    path('', mainpage, name='main-page'),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/profile/', profile, name='profile-page'),
    path('Organisation/', OrganisationList.as_view(), name='organisation-list'),
    path('Organisation/add/', OrganisationCreate.as_view(), name='organisation-add'),
    path('Organisation/<int:pk>/', OrganisationUpdate.as_view(), name='organisation-update'),
    path('Organisation/<int:pk>/delete/', OrganisationDelete.as_view(), name='organisation-delete'),
    path('Contact/', ContactList.as_view(), name='contact-list'),
    path('Contact/add/<int:pk>/', ContactCreate.as_view(), name='contact-add'),
    path('bezoeker/', RedirectView.as_view(url='https://covidus.nl/Contact/add/1/', permanent=False), name='quick'),
    path('success/', success, name='success-page'),
    # path('Public/add/<int:pk>/', PublicKeyCreate.as_view(), name='public-key-add'),
]
