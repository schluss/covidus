# Covidus

Covidus is an [Django](https://www.djangoproject.com/) webapplication to register vistors at an establishment (see [usage](https://gitlab.com/schluss/covidus/-/edit/master/README.md#usage-and-privacy-protection) for how this works). It guarantees [privacy protection](https://gitlab.com/schluss/covidus/-/edit/master/README.md#usage-and-privacy-protection) by encrypting vistor data client side (on the clients browser).

## Installation

Use the container software [docker-compose](https://docs.docker.com/compose/) to bring up the development environment.

```bash
docker-compose up
```

## Demo
<p> Video of a vistor registering afte scanning an QR-code:

<img src="https://gitlab.com/schluss/covidus/-/raw/master/doc/register_contact.gif" width="50%">

<p>Data is being encrypted in the clients browser and what get's stored in the database is this:</p>

<img src="https://gitlab.com/schluss/covidus/-/raw/master/doc/screenshot.png" width="40%">


## Usage and privacy protection
Vistors scan an QR-code that brings their browser to a webform. Vistors fill in that webform which is then encrypted with a public key belonging to the establishment that is being visted by the vistor. The encrypted data is then send to the server and stored in a database. 

When public health services need the data for contact investigations they will receive the encrypted data from [Schluss](https://schluss.org) and the private key from the establishment.

Both parties are needed to view the data. No party has both pieces until they are physically brought together by the parties
involved.

## Design
Covidus uses [privacy-by-design](https://en.wikipedia.org/wiki/Privacy_by_design) by encorporating the guidelines put forward by the [Radboud Univerity](https://blog.xot.nl/2012/09/10/eight-privacy-design-strategies/).

Please have a look at the [wiki](https://gitlab.com/schluss/covidus/-/wikis/home) to see how this app is designed and for a list of stuff to work on.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)